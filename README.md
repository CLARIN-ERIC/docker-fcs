## Running

Run the project with docker.

#### Initilisation

Create volumes:
```
docker volume create fcs-data
docker volume create fcs-data-backup
```

#### Openjdk8
```
docker run -ti --rm \
    --name=fcs \
    -p 4019:4019 \
    -v fcs-data:/data \
    -v fcs-data-backup:/data-bakup \
    docker-fcs:c74112c
```

The application is available on http://localhost:4019.

#### Openjdk11
```
docker run -ti --rm \
    --name=fcs \
    -p 4019:4019 \
    -v fcs-data:/data \
    -v fcs-data-backup:/data-bakup \
    docker-fcs:c74112c
```

The application is available on http://localhost:4019.